USERNAME := xulxc
IMAGE := docker__php7.3_oci8
VERSION := 0.1

build:
	@docker build --no-cache -t $(IMAGE) .
	@make push
	@make -s clean

up:
	@docker-compose up

push:
	@docker tag $(IMAGE) registry.gitlab.com/$(USERNAME)/$(IMAGE):$(VERSION)
	@docker push registry.gitlab.com/$(USERNAME)/$(IMAGE):$(VERSION)

clean:
	@docker system prune --volumes --force -a
