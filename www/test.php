<?php error_reporting(E_ERROR);

// https://docs.oracle.com/en/cloud/paas/autonomous-data-warehouse-cloud/user/connecting-nodejs.html
$connection = function () {
    $oci = oci_connect(getenv('DB_USER'), getenv('DB_PASSWORD'), getenv('DB_RES'));

    if (false === $oci) {
        throw new \Exception(oci_error()['message']);
    }

    return $oci;
};

$version = function () use (&$connection): string {
    return oci_server_version($connection);
};


$query = function (string $query) use (&$connection): ?array {
    $parse = oci_parse($connection, $query);
    oci_execute($parse);
    $result = oci_fetch_array($parse, OCI_ASSOC);

    if (false === $result) {
        // Database query returns an empty result set
        return null;
    }

    return $result;
};

try {
    $connection = $connection();
    echo $version() . "<br>";
    $result = $query('SELECT * FROM test');
    var_dump($result);
} catch (\Exception $e) {
    throw new Error($e->getMessage());
}
