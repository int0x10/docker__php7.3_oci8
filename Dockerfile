FROM debian:buster
WORKDIR /opt/oracle

RUN apt update && apt install wget unzip -y

RUN wget https://download.oracle.com/otn_software/linux/instantclient/193000/instantclient-basic-linux.x64-19.3.0.0.0dbru.zip && \
    wget https://download.oracle.com/otn_software/linux/instantclient/193000/instantclient-sdk-linux.x64-19.3.0.0.0dbru.zip

RUN unzip instantclient-basic-linux.x64-19.3.0.0.0dbru.zip && \
    unzip instantclient-sdk-linux.x64-19.3.0.0.0dbru.zip

RUN apt update && apt install php-dev php-pear build-essential libaio1 -y

RUN echo 'instantclient,/opt/oracle/instantclient_19_3/' | pecl install oci8


# https://docs.docker.com/develop/develop-images/multistage-build/
FROM php:7.3-fpm
ENV LD_LIBRARY_PATH /opt/oracle/instantclient_19_3/:${LD_LIBRARY_PATH}
RUN apt update && apt install libaio1 -y

COPY --from=0 /usr/lib/php/20180731/oci8.so /usr/local/lib/php/extensions/no-debug-non-zts-20180731/oci8.so
COPY --from=0 /opt/oracle/instantclient_19_3 /opt/oracle/instantclient_19_3

RUN echo "extension=oci8.so" > /usr/local/etc/php/conf.d/99-oci8.ini